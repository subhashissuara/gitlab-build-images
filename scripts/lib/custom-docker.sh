# Note: Check out https://wiki.bash-hackers.org/syntax/pe for documentation on
# various variable operations used in this script.

PATH_TOOLS=(DEBIAN OS UBI RUBY GOLANG RUST NODE POSTGRESQL)
TAG_TOOLS=(BUNDLER RUBYGEMS GIT LFS CHROME YARN GRAPHICSMAGICK EXIFTOOL BAZELISK DOCKER GCLOUD KUBECTL HELM)

# Generate the docker image path using the components that were specified via
# variables.
# For example, consider a CI job which specifies the following variables:
# OS: debian:bullseye
# RUBY: 3.0
# GOLANG: 1.19
# RUST: 1.73.0
# GIT: 2.33
# POSTGRESQL: 11
# With the above variables, this function will return
# `debian-bullseye-ruby-2.7-golang-1.19-postgresql-11`
function get_image_path() {
    local path
    path=""
    for tool in "${PATH_TOOLS[@]}"; do
        if [[ -n "${!tool}" ]]; then
            if [[ "${tool}" == "OS" ]]; then
                # The OS variable's value is following <distro>:<version>
                # format. We split that string into individual components.
                distro=${!tool%:*}
                version=${!tool#*:}
                path="${path}-${distro}-${version}"
            else
                # Convert the tool name into lowercase using `,,` operator
                path="${path}-${tool,,}-${!tool}"
            fi
        fi
    done

    if [[ -n "$path" ]]; then
        echo "$CI_REGISTRY_IMAGE/${path:1}"
    else
        echo "$CI_REGISTRY_IMAGE"
    fi
}

# Generate the image tag using the components that were specified via variables.
# For example, consider a CI job which specifies the following variables:
# OS: debian:bullseye
# RUBY: 2.7
# GOLANG: 1.19
# RUST: 1.73.0
# GIT: 2.33
# POSTGRESQL: 11
# For that job, this function will return
# `git-2.33`
function get_image_tag() {
    local tag
    tag=""
    for tool in "${TAG_TOOLS[@]}"; do
        if [[ -n "${!tool}" ]]; then
            # Convert the tool name into lowercase using `,,` operator
            tag="${tag}-${tool,,}-${!tool}"
        fi
    done

    if [[ -n "$tag" ]]; then
        echo "${tag:1}"
    else
        echo "latest"
    fi
}
