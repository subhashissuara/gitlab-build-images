#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

function build_debian() {
    export DEBIAN_FRONTEND=noninteractive

    # echo "deb http://deb.debian.org/debian testing main" | tee -a /etc/apt/sources.list.d/testing.list
    # Install packages
    apt-get update
    apt-get install -yq --no-install-recommends \
        make gcc g++ locales \
        rsync git-core \
        ed file curl gnupg2 \
        yamllint unzip \
        patch

    # Install Imagemagick for cropping the pictures on the team page
    apt-get install -yq --no-install-recommends imagemagick

    # Install Hashicorp Vault
    curl -s https://apt.releases.hashicorp.com/gpg | apt-key --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg add -
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com bullseye main" | tee /etc/apt/sources.list.d/hashicorp.list
    apt-get update
    apt-get install -yq --no-install-recommends vault

    # Install node & yarn
    /scripts/install-node "${NODE_INSTALL_VERSION}" "${YARN_INSTALL_VERSION}"

    # Install gitlab-runner
    curl -O -J -L "https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-${TARGETARCH:-amd64}"
    mv gitlab-ci-multi-runner-linux-amd64 /usr/bin/gitlab-runner-helper
    chmod +x /usr/bin/gitlab-runner-helper

    # Patch gsutil to support gzip compression with rsync command:
    # https://github.com/GoogleCloudPlatform/gsutil/pull/1430
    if [[ -d "/patches/gsutil" ]]; then
    for i in /patches/gsutil/*.patch; do
        echo "$i..."
        patch -d /usr/lib/google-cloud-sdk/platform/gsutil -p1 -i "$i"
    done
    fi

    # Set UTF-8
    echo "C.UTF-8 UTF-8" > /etc/locale.gen
    locale-gen
    update-locale LANG=C.UTF-8 LC_CTYPE=C.UTF-8 LC_ALL=C.UTF-8
    locale -a

    # Clean up
    apt-get autoremove -yq
    apt-get clean -yqq
    rm -rf /var/lib/apt/lists/*
}
BUILD_OS=${BUILD_OS:-debian}

if [[ $BUILD_OS =~ debian ]]; then
    build_debian "$@"
elif [[ $BUILD_OS =~ ubi ]]; then
    build_ubi "$@"
fi


