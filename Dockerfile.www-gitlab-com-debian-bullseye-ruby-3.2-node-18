ARG GCLOUD_VERSION=413.0.0

# Google-cloud-sdk
#
# gsutil 5.18 unnecessarily requires the storage.buckets.get
# permission: https://github.com/GoogleCloudPlatform/gsutil/issues/1663
FROM gcr.io/google.com/cloudsdktool/cloud-sdk:$GCLOUD_VERSION as gcloud-sdk
FROM ruby:3.2.2-slim-bullseye

# Install Google Cloud SDK for deploys via rsync
COPY --from=gcloud-sdk /usr/lib/google-cloud-sdk /usr/lib/google-cloud-sdk
COPY --from=gcloud-sdk /usr/share/google-cloud-sdk /usr/share/google-cloud-sdk
RUN cd /usr/bin && find ../lib/google-cloud-sdk/bin -type f -executable -exec ln -s {} \;; cd -

ADD /scripts/ /scripts/
ADD /patches /patches/

ENV NODE_INSTALL_VERSION=18.17.0
ENV YARN_INSTALL_VERSION=1.22.19
RUN /scripts/install-www-gitlab-com

# Set UTF-8 http://jaredmarkell.com/docker-and-locales/
# Must be set after install-essentials is run
ENV LANG C.UTF-8
ENV LANGUAGE C
ENV LC_ALL C.UTF-8
